## Run Docker image with Dockerfile
docker run -d -p <host_port>:8080 tanquyvt/simpleservlet:multi-stage

(-d: detached mode)

## Run Docker image with docker-compose

Go inside the top folder of your project with command line.

1. docker swarm init
2. docker stack deploy -c docker-compose.yml webapptest
3. Access (http://localhost:4000//MyWebTest/start-web-test)
