# Stage 1: Build app
# Use an official runtime as a parent image
FROM tomcat:8.5.42-jdk8-openjdk-slim AS BUILDER

# Set the working directory
WORKDIR /app

COPY . /app

RUN javac -classpath $CATALINA_HOME/lib/servlet-api.jar ./src/WebTest.java

RUN cp ./src/WebTest.class ./deploy/WEB-INF/classes

RUN jar cf ./deploy/MyWebTest.war -C deploy .

# Stage 2: Copy from builder image

FROM tomcat:8.5-alpine

COPY --from=BUILDER /app/deploy/MyWebTest.war $CATALINA_HOME/webapps

# Make port available to the world outside this container
EXPOSE 8080

# Run when the container launches
CMD ["catalina.sh", "run"]