import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class WebTest extends HttpServlet {

  public void doGet(HttpServletRequest request, HttpServletResponse response) 
    throws IOException, ServletException 
  {
    response.setContentType("text/html");
    PrintWriter writer = response.getWriter();

    writer.println("<html><head><title>Simple Web Application</title></head>");
    writer.println("<body><h1>A Simple Web Application</h1>");
    writer.println("Here's some text...<br>...and a bit more text");
    writer.println("</body></html>");
  }
}